/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ig.client.http;

import com.ig.hashtag.dto.Graphql;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author MyAdmin
 */
@Component
public class IGClient {

    @Autowired
    @Qualifier("instagramRT")
    private RestTemplate restTemplate;

    private boolean isOK(ResponseEntity<?> resp) {
        return resp.getStatusCodeValue() >= 200 && resp.getStatusCodeValue() < 300;
    }

    private <T> T getBody(ResponseEntity<T> resp) {
        if (isOK(resp)) {
            return resp.getBody();
        } else {
            if (resp.hasBody()) {
                return resp.getBody();
            } else {
                return null;
            }
        }
    }

    public Graphql getPostByHashtag(String hashtag) {
        String path = "/explore/tags/" + hashtag + "/";
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromPath(path);
        uriBuilder.queryParam("__a", "1");

        ResponseEntity<Graphql> resp = restTemplate.exchange(uriBuilder.build().toUriString(), HttpMethod.GET, null,
                new ParameterizedTypeReference<Graphql>() {
        });
        return getBody(resp);
    }

    public String getProfileByUserID(String userId) {
        String path = "web/friendships/" + userId + "/follow";
        ResponseEntity<String> resp = restTemplate.exchange(path, HttpMethod.GET, null,
                new ParameterizedTypeReference<String>() {
        });
        return resp.getHeaders().getLocation().toString();
    }

    public Graphql getProfileByUsername(String username) {
        String path = username + "/?__a=1";
        ResponseEntity<Graphql> resp = restTemplate.exchange(path, HttpMethod.GET, null,
                new ParameterizedTypeReference<Graphql>() {
        });
        return getBody(resp);
    }

}
