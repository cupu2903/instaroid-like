package com.ig.hashtag.dto;

import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("graphql")
public class Graphql{
	private Hashtag hashtag;

	public void setHashtag(Hashtag hashtag){
		this.hashtag = hashtag;
	}

	public Hashtag getHashtag(){
		return hashtag;
	}

	@Override
 	public String toString(){
		return 
			"Graphql{" + 
			"hashtag = '" + hashtag + '\'' + 
			"}";
		}
}
