package com.ig.hashtag.dto;

public class Response{
	private Graphql graphql;

	public void setGraphql(Graphql graphql){
		this.graphql = graphql;
	}

	public Graphql getGraphql(){
		return graphql;
	}

	@Override
 	public String toString(){
		return 
			"ResponsePPO{" + 
			"graphql = '" + graphql + '\'' + 
			"}";
		}
}
