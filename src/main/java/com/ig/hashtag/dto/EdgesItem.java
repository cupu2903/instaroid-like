package com.ig.hashtag.dto;

public class EdgesItem{
	private Node node;

	public void setNode(Node node){
		this.node = node;
	}

	public Node getNode(){
		return node;
	}

	@Override
 	public String toString(){
		return 
			"EdgesItem{" + 
			"node = '" + node + '\'' + 
			"}";
		}
}
