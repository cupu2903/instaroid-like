package com.ig.hashtag.dto;

public class EdgeMediaPreviewLike{
	private int count;

	public void setCount(int count){
		this.count = count;
	}

	public int getCount(){
		return count;
	}

	@Override
 	public String toString(){
		return 
			"EdgeMediaPreviewLike{" + 
			"count = '" + count + '\'' + 
			"}";
		}
}
