package com.ig.hashtag.dto;

public class Dimensions{
	private int width;
	private int height;

	public void setWidth(int width){
		this.width = width;
	}

	public int getWidth(){
		return width;
	}

	public void setHeight(int height){
		this.height = height;
	}

	public int getHeight(){
		return height;
	}

	@Override
 	public String toString(){
		return 
			"Dimensions{" + 
			"width = '" + width + '\'' + 
			",height = '" + height + '\'' + 
			"}";
		}
}
