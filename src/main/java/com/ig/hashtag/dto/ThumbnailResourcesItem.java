package com.ig.hashtag.dto;

public class ThumbnailResourcesItem{
	private String src;
	private int configHeight;
	private int configWidth;

	public void setSrc(String src){
		this.src = src;
	}

	public String getSrc(){
		return src;
	}

	public void setConfigHeight(int configHeight){
		this.configHeight = configHeight;
	}

	public int getConfigHeight(){
		return configHeight;
	}

	public void setConfigWidth(int configWidth){
		this.configWidth = configWidth;
	}

	public int getConfigWidth(){
		return configWidth;
	}

	@Override
 	public String toString(){
		return 
			"ThumbnailResourcesItem{" + 
			"src = '" + src + '\'' + 
			",config_height = '" + configHeight + '\'' + 
			",config_width = '" + configWidth + '\'' + 
			"}";
		}
}
