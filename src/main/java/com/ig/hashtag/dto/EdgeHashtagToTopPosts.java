package com.ig.hashtag.dto;

import java.util.List;

public class EdgeHashtagToTopPosts{
	private List<EdgesItem> edges;

	public void setEdges(List<EdgesItem> edges){
		this.edges = edges;
	}

	public List<EdgesItem> getEdges(){
		return edges;
	}

	@Override
 	public String toString(){
		return 
			"EdgeHashtagToTopPosts{" + 
			"edges = '" + edges + '\'' + 
			"}";
		}
}