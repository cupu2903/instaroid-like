package com.ig.hashtag.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)

public class Node {

    private Owner owner;
    private EdgeMediaToComment edgeMediaToComment;
    private String displayUrl;
    private String thumbnailSrc;
    private Long takenAtTimestamp;
    private String shortcode;
    private EdgeLikedBy edgeLikedBy;
    private boolean isVideo;
    private boolean commentsDisabled;
    private String id;
    private EdgeMediaToCaption edgeMediaToCaption;
    private List<ThumbnailResourcesItem> thumbnailResources;
    private Dimensions dimensions;
    private EdgeMediaPreviewLike edgeMediaPreviewLike;
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setEdgeMediaToComment(EdgeMediaToComment edgeMediaToComment) {
        this.edgeMediaToComment = edgeMediaToComment;
    }

    public EdgeMediaToComment getEdgeMediaToComment() {
        return edgeMediaToComment;
    }

    public void setDisplayUrl(String displayUrl) {
        this.displayUrl = displayUrl;
    }

    public String getDisplayUrl() {
        return displayUrl;
    }

    public void setThumbnailSrc(String thumbnailSrc) {
        this.thumbnailSrc = thumbnailSrc;
    }

    public String getThumbnailSrc() {
        return thumbnailSrc;
    }

    public Long getTakenAtTimestamp() {
        return takenAtTimestamp;
    }

    public void setTakenAtTimestamp(Long takenAtTimestamp) {
        this.takenAtTimestamp = takenAtTimestamp;
    }

    public void setShortcode(String shortcode) {
        this.shortcode = shortcode;
    }

    public String getShortcode() {
        return shortcode;
    }

    public void setEdgeLikedBy(EdgeLikedBy edgeLikedBy) {
        this.edgeLikedBy = edgeLikedBy;
    }

    public EdgeLikedBy getEdgeLikedBy() {
        return edgeLikedBy;
    }

    public void setIsVideo(boolean isVideo) {
        this.isVideo = isVideo;
    }

    public boolean isIsVideo() {
        return isVideo;
    }

    public void setCommentsDisabled(boolean commentsDisabled) {
        this.commentsDisabled = commentsDisabled;
    }

    public boolean isCommentsDisabled() {
        return commentsDisabled;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setEdgeMediaToCaption(EdgeMediaToCaption edgeMediaToCaption) {
        this.edgeMediaToCaption = edgeMediaToCaption;
    }

    public EdgeMediaToCaption getEdgeMediaToCaption() {
        return edgeMediaToCaption;
    }

    public void setThumbnailResources(List<ThumbnailResourcesItem> thumbnailResources) {
        this.thumbnailResources = thumbnailResources;
    }

    public List<ThumbnailResourcesItem> getThumbnailResources() {
        return thumbnailResources;
    }

    public void setDimensions(Dimensions dimensions) {
        this.dimensions = dimensions;
    }

    public Dimensions getDimensions() {
        return dimensions;
    }

    public void setEdgeMediaPreviewLike(EdgeMediaPreviewLike edgeMediaPreviewLike) {
        this.edgeMediaPreviewLike = edgeMediaPreviewLike;
    }

    public EdgeMediaPreviewLike getEdgeMediaPreviewLike() {
        return edgeMediaPreviewLike;
    }


    @Override
    public String toString() {
        return "Node{"
                + "owner = '" + owner + '\''
                + ",edge_media_to_comment = '" + edgeMediaToComment + '\''
                + ",display_url = '" + displayUrl + '\''
                + ",thumbnail_src = '" + thumbnailSrc + '\''
                + ",taken_at_timestamp = '" + takenAtTimestamp + '\''
                + ",shortcode = '" + shortcode + '\''
                + ",edge_liked_by = '" + edgeLikedBy + '\''
                + ",is_video = '" + isVideo + '\''
                + ",comments_disabled = '" + commentsDisabled + '\''
                + ",id = '" + id + '\''
                + ",edge_media_to_caption = '" + edgeMediaToCaption + '\''
                + ",thumbnail_resources = '" + thumbnailResources + '\''
                + ",dimensions = '" + dimensions + '\''
                + ",edge_media_preview_like = '" + edgeMediaPreviewLike + '\''
                + "}";
    }
}
