package com.ig.hashtag.dto;

import java.util.List;

public class EdgeMediaToCaption{
	private List<EdgesItem> edges;

	public void setEdges(List<EdgesItem> edges){
		this.edges = edges;
	}

	public List<EdgesItem> getEdges(){
		return edges;
	}

	@Override
 	public String toString(){
		return 
			"EdgeMediaToCaption{" + 
			"edges = '" + edges + '\'' + 
			"}";
		}
}