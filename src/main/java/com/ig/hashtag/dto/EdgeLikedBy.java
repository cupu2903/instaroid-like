package com.ig.hashtag.dto;

public class EdgeLikedBy{
	private int count;

	public void setCount(int count){
		this.count = count;
	}

	public int getCount(){
		return count;
	}

	@Override
 	public String toString(){
		return 
			"EdgeLikedBy{" + 
			"count = '" + count + '\'' + 
			"}";
		}
}
