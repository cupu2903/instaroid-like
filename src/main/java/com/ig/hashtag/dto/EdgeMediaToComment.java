package com.ig.hashtag.dto;

public class EdgeMediaToComment{
	private int count;

	public void setCount(int count){
		this.count = count;
	}

	public int getCount(){
		return count;
	}

	@Override
 	public String toString(){
		return 
			"EdgeMediaToComment{" + 
			"count = '" + count + '\'' + 
			"}";
		}
}
