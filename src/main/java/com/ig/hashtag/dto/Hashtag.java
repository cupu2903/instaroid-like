package com.ig.hashtag.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Hashtag{
	private EdgeHashtagToMedia edgeHashtagToMedia;
	private boolean isTopMediaOnly;
	private EdgeHashtagToContentAdvisory edgeHashtagToContentAdvisory;
	private String name;
	private boolean isFollowing;
	private String profilePicUrl;
	private EdgeHashtagToTopPosts edgeHashtagToTopPosts;

	public void setEdgeHashtagToMedia(EdgeHashtagToMedia edgeHashtagToMedia){
		this.edgeHashtagToMedia = edgeHashtagToMedia;
	}

	public EdgeHashtagToMedia getEdgeHashtagToMedia(){
		return edgeHashtagToMedia;
	}

	public void setIsTopMediaOnly(boolean isTopMediaOnly){
		this.isTopMediaOnly = isTopMediaOnly;
	}

	public boolean isIsTopMediaOnly(){
		return isTopMediaOnly;
	}

	public void setEdgeHashtagToContentAdvisory(EdgeHashtagToContentAdvisory edgeHashtagToContentAdvisory){
		this.edgeHashtagToContentAdvisory = edgeHashtagToContentAdvisory;
	}

	public EdgeHashtagToContentAdvisory getEdgeHashtagToContentAdvisory(){
		return edgeHashtagToContentAdvisory;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setIsFollowing(boolean isFollowing){
		this.isFollowing = isFollowing;
	}

	public boolean isIsFollowing(){
		return isFollowing;
	}

	public void setProfilePicUrl(String profilePicUrl){
		this.profilePicUrl = profilePicUrl;
	}

	public String getProfilePicUrl(){
		return profilePicUrl;
	}

	public void setEdgeHashtagToTopPosts(EdgeHashtagToTopPosts edgeHashtagToTopPosts){
		this.edgeHashtagToTopPosts = edgeHashtagToTopPosts;
	}

	public EdgeHashtagToTopPosts getEdgeHashtagToTopPosts(){
		return edgeHashtagToTopPosts;
	}

	@Override
 	public String toString(){
		return 
			"Hashtag{" + 
			"edge_hashtag_to_media = '" + edgeHashtagToMedia + '\'' + 
			",is_top_media_only = '" + isTopMediaOnly + '\'' + 
			",edge_hashtag_to_content_advisory = '" + edgeHashtagToContentAdvisory + '\'' + 
			",name = '" + name + '\'' + 
			",is_following = '" + isFollowing + '\'' + 
			",profile_pic_url = '" + profilePicUrl + '\'' + 
			",edge_hashtag_to_top_posts = '" + edgeHashtagToTopPosts + '\'' + 
			"}";
		}
}
