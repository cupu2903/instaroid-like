package com.ig.hashtag.dto;

public class PageInfo{
	private boolean hasNextPage;
	private String endCursor;

	public void setHasNextPage(boolean hasNextPage){
		this.hasNextPage = hasNextPage;
	}

	public boolean isHasNextPage(){
		return hasNextPage;
	}

	public void setEndCursor(String endCursor){
		this.endCursor = endCursor;
	}

	public String getEndCursor(){
		return endCursor;
	}

	@Override
 	public String toString(){
		return 
			"PageInfo{" + 
			"has_next_page = '" + hasNextPage + '\'' + 
			",end_cursor = '" + endCursor + '\'' + 
			"}";
		}
}
