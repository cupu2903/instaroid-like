package com.ig.hashtag.dto;

import java.util.List;

public class EdgeHashtagToMedia{
	private PageInfo pageInfo;
	private int count;
	private List<EdgesItem> edges;

	public void setPageInfo(PageInfo pageInfo){
		this.pageInfo = pageInfo;
	}

	public PageInfo getPageInfo(){
		return pageInfo;
	}

	public void setCount(int count){
		this.count = count;
	}

	public int getCount(){
		return count;
	}

	public void setEdges(List<EdgesItem> edges){
		this.edges = edges;
	}

	public List<EdgesItem> getEdges(){
		return edges;
	}

	@Override
 	public String toString(){
		return 
			"EdgeHashtagToMedia{" + 
			"page_info = '" + pageInfo + '\'' + 
			",count = '" + count + '\'' + 
			",edges = '" + edges + '\'' + 
			"}";
		}
}