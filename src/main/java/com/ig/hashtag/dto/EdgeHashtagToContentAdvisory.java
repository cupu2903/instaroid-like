package com.ig.hashtag.dto;

import java.util.List;

public class EdgeHashtagToContentAdvisory{
	private int count;
	private List<Object> edges;

	public void setCount(int count){
		this.count = count;
	}

	public int getCount(){
		return count;
	}

	public void setEdges(List<Object> edges){
		this.edges = edges;
	}

	public List<Object> getEdges(){
		return edges;
	}

	@Override
 	public String toString(){
		return 
			"EdgeHashtagToContentAdvisory{" + 
			"count = '" + count + '\'' + 
			",edges = '" + edges + '\'' + 
			"}";
		}
}