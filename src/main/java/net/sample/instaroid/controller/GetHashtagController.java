/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.sample.instaroid.controller;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;
import net.sample.instaroid.dto.HashtagDto;
import net.sample.instaroid.mapper.ModelMapper;
import net.sample.instaroid.service.HashtagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MyAdmin
 */
@RestController
@RequestMapping(value = "/hashtag")
public class GetHashtagController {

    @Autowired
    HashtagService hashtagService;
    @Autowired
    ModelMapper modelMapper;

    @GetMapping
    public List<HashtagDto> getHashtag(Principal principal,
            @RequestParam(value = "q", required = false) String hashtag) {
        return hashtagService.getHashtag(hashtag).stream().map(modelMapper::asHashtagDto).collect(Collectors.toList());
    }

}
