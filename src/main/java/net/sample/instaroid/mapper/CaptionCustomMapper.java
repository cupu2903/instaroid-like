/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.sample.instaroid.mapper;

import com.ig.hashtag.dto.EdgesItem;
import java.util.List;
import org.springframework.stereotype.Component;

/**
 *
 * @author MyAdmin
 */
@Component
public class CaptionCustomMapper {

    public String contentAsSnippet(List<EdgesItem> edges) {
        if (edges != null) {
            return edges.get(0).getNode().getText();
        } else {
            return null;
        }
    }
}
