/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.sample.instaroid.mapper;

import com.ig.hashtag.dto.EdgesItem;
import fr.xebia.extras.selma.Field;
import fr.xebia.extras.selma.IgnoreMissing;
import fr.xebia.extras.selma.IoC;
import fr.xebia.extras.selma.Mapper;
import fr.xebia.extras.selma.Maps;
import net.sample.instaroid.dto.HashtagDto;

/**
 *
 * @author MyAdmin
 */
@Mapper(withIgnoreMissing = IgnoreMissing.ALL, withIoC = IoC.SPRING)
public interface ModelMapper {

    @Maps(withCustomFields = {
        @Field({"EdgesItem.node.owner.id", "HashtagDto.ownerId"})
        ,@Field({"EdgesItem.node.id", "HashtagDto.postId"})
        ,@Field(value = {"EdgesItem.node.displayUrl", "HashtagDto.displayPict"})
        ,@Field({"EdgesItem.node.edgeLikedBy.count", "HashtagDto.likeCnt"})
        ,@Field(value = {"EdgesItem.node.edgeMediaToCaption.edges", "HashtagDto.caption"}, withCustom = CaptionCustomMapper.class)
        ,@Field({"EdgesItem.node.edgeMediaToComment.count", "HashtagDto.commentCnt"})
        ,@Field({"EdgesItem.node.edgeMediaPreviewLike.count", "HashtagDto.viewerCnt"})
        , @Field({"EdgesItem.node.takenAtTimestamp", "HashtagDto.takenAt"})})
    HashtagDto asHashtagDto(EdgesItem edgeItem);

}
