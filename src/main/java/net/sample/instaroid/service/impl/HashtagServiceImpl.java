/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.sample.instaroid.service.impl;

import com.ig.client.http.IGClient;
import com.ig.hashtag.dto.EdgeHashtagToMedia;
import com.ig.hashtag.dto.EdgesItem;
import com.ig.hashtag.dto.Graphql;
import java.util.List;
import net.sample.instaroid.persistence.model.Hashtag;
import net.sample.instaroid.service.HashtagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author MyAdmin
 */
@Service
@Transactional
public class HashtagServiceImpl implements HashtagService {

    @Autowired
    IGClient iGClient;

    @Override
    public List<EdgesItem> getHashtag(String hashtag) {
        Graphql postByHashtag = iGClient.getPostByHashtag(hashtag);
        EdgeHashtagToMedia edgeHashtagToMedia = postByHashtag.getHashtag().getEdgeHashtagToMedia();
        List<EdgesItem> edges = edgeHashtagToMedia.getEdges();

        System.out.println("graphql" + postByHashtag.toString());
        return edges;

    }

    @Override
    public Hashtag findById(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Hashtag> findAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Page<Hashtag> findPaginated(int page, int size) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Hashtag create(Hashtag entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Hashtag update(Hashtag entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Hashtag entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteById(String entityId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
