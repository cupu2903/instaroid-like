/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.sample.instaroid.service;

import com.ig.hashtag.dto.EdgesItem;
import java.util.List;
import net.sample.instaroid.persistence.IOperations;
import net.sample.instaroid.persistence.model.Hashtag;

/**
 *
 * @author MyAdmin
 */
public interface HashtagService extends IOperations<Hashtag, String>{
    
    public List<EdgesItem> getHashtag(String hashtag);
    
}
