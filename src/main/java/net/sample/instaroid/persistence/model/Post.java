/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.sample.instaroid.persistence.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author MyAdmin
 */
@Entity
public class Post implements Serializable {

    @Id
    @Column(name = "post_id")
    private String postId;
    @Column(name = "hashtag")
    private String hashtag;
    @Column(name = "userid")
    private String userId;
    @Column(name = "like_cnt")
    private int likeCnt;
    @Column(name = "comment_cnt")
    private int commentCnt;
    @Column(name = "preview_cnt")
    private int previewCnt;
    @Column(name = "caption")
    private String caption;
    @Column(name = "display_pict")
    private String displayPict;
    @Column(name = "thumbnail")
    private String thumbnail;
    @Column(name = "is_video")
    private boolean isVideo;
    @Column(name = "taken_at")
    private Date takenAt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userid", insertable = false, updatable = false)
    private TbUser tbUser;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "hashtag", insertable = false, updatable = false)
    private Hashtag hastag;

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getHashtag() {
        return hashtag;
    }

    public void setHashtag(String hashtag) {
        this.hashtag = hashtag;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getLikeCnt() {
        return likeCnt;
    }

    public void setLikeCnt(int likeCnt) {
        this.likeCnt = likeCnt;
    }

    public int getCommentCnt() {
        return commentCnt;
    }

    public void setCommentCnt(int commentCnt) {
        this.commentCnt = commentCnt;
    }

    public int getPreviewCnt() {
        return previewCnt;
    }

    public void setPreviewCnt(int previewCnt) {
        this.previewCnt = previewCnt;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getDisplayPict() {
        return displayPict;
    }

    public void setDisplayPict(String displayPict) {
        this.displayPict = displayPict;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public boolean isIsVideo() {
        return isVideo;
    }

    public void setIsVideo(boolean isVideo) {
        this.isVideo = isVideo;
    }

    public Date getTakenAt() {
        return takenAt;
    }

    public void setTakenAt(Date takenAt) {
        this.takenAt = takenAt;
    }

    public TbUser getTbUser() {
        return tbUser;
    }

    public void setTbUser(TbUser tbUser) {
        this.tbUser = tbUser;
    }

    public Hashtag getHastag() {
        return hastag;
    }

    public void setHastag(Hashtag hastag) {
        this.hastag = hastag;
    }


}
