package net.sample.instaroid.persistence.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import net.sample.instaroid.persistence.model.Post;

public interface PostDao extends JpaRepository<Post, Integer> {

    Post findByHashtag(String hashtag);

}
