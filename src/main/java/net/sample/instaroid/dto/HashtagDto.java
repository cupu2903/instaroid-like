/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.sample.instaroid.dto;

/**
 *
 * @author MyAdmin
 */
public class HashtagDto {

    private String ownerId;
    private String postId;
    private String displayPict;
    private String caption;
    private int likeCnt;
    private int commentCnt;
    private int viewerCnt;
    private Long takenAt;

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getDisplayPict() {
        return displayPict;
    }

    public void setDisplayPict(String displayPict) {
        this.displayPict = displayPict;
    }

    public int getLikeCnt() {
        return likeCnt;
    }

    public void setLikeCnt(int likeCnt) {
        this.likeCnt = likeCnt;
    }

    public int getCommentCnt() {
        return commentCnt;
    }

    public void setCommentCnt(int commentCnt) {
        this.commentCnt = commentCnt;
    }

    public int getViewerCnt() {
        return viewerCnt;
    }

    public void setViewerCnt(int viewerCnt) {
        this.viewerCnt = viewerCnt;
    }

    public Long getTakenAt() {
        return takenAt;
    }

    public void setTakenAt(Long takenAt) {
        this.takenAt = takenAt;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

}
