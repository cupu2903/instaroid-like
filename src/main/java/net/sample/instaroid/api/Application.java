/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.sample.instaroid.api;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author MyAdmin
 */
@ComponentScan({"net.sample.instaroid", "net.sample.instaroid.mapper", "com.ig.client.http"})
@EnableAutoConfiguration
@SpringBootApplication
@PropertySource({ "classpath:instagram.properties" })
public class Application {
    
	@Autowired
	private Environment env;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean(name = "instagramRT")
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        // @formatter:off
        MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter(
                new ObjectMapper()
                        .setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE)
                        .enable(DeserializationFeature.UNWRAP_ROOT_VALUE));
        return builder
                .rootUri("https://www.instagram.com")
                .setConnectTimeout(15 * 1000).setReadTimeout(60 * 1000)
                .messageConverters(Arrays.asList(messageConverter, new FormHttpMessageConverter()))
                .additionalInterceptors((request, body, execution) -> {
                    request.getHeaders().add("cookie", env.getProperty("instagram.cookie"));
                    return execution.execute(request, body);
                }).build();
        // @formatter:on
    }

}
